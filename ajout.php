<?php
require_once "ServerParam.php";
require_once "ReqMng.php";
require_once "req.php";
require_once "navbar.php";


if (!empty($_COOKIE['mail']) && !empty($_COOKIE['nom']) && !empty($_COOKIE['prenom']) && !empty($_COOKIE['departement'])) {
    $nom = $_COOKIE['nom'];
    $prenom = $_COOKIE['prenom'];
    $mail = $_COOKIE['mail'];
    $departement = $_COOKIE['departement'];
}


?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.0.2/tailwind.min.css" integrity="sha512-+WF6UMXHki/uCy0vATJzyA9EmAcohIQuwpNz0qEO+5UeE5ibPejMRdFuARSrl1trs3skqie0rY/gNiolfaef5w==" crossorigin="anonymous" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./style/style.css" />
    <title>PHP DATING</title>
</head>

<body class="flex flex-col justify-center text-center">

    <?php
    navbar();

    //appelle getPDO pour initier les transactions
    $bdd = getPDO();

    if (empty($_POST)) :


    ?>

        <section>
            <div class="m-5"><button class="rounded bg-blue-300 font-bold hover:bg-blue-400 transition delay-150 duration-300 ease-in-out transform hover:scale-110 w-auto h-10 px-4" name="accueil"><a href='./index.php'>ACCUEIL</a></button></div>
            <div class="md:grid md:grid-cols-12 md:gap-2 mx-2 pb-8">
                <div class="md:col-start-5 md:col-end-9 mt-10">
                    <form action="ajout.php" method="post" class="bg-blue-100 rounded shadow-md p-4 mt-10" name="form_ajout_sport">
                        <h2 class="text-center font-bold">Rajoutez votre sport favori :</h2>
                        <label class="font-bold">Autre Sport:</label>
                        <input type="text" name="autre" class="border-double border-4 border-light-blue-500 m-4 placeholder-blue-400 placeholder-opacity-50 px-4" placeholder="Sport à rajouter">
                        <input type="submit" name="ajout" value="Ajouter" class="rounded bg-pink-300 font-bold hover:bg-pink-400 transition delay-150 duration-300 ease-in-out transform hover:scale-110 w-auto h-10 px-4 my-5 md:mt-10" />
                    </form>
                </div>
            </div>
            <div class="md:grid md:grid-cols-12 md:gap-2 mx-2 pb-8">
                <div class="md:col-end-10 md:col-span-6 bg-gray-200 rounded my-10 md:mt-10 pb-30 shadow-md">
                    <h1 class="text-center font-bold my-4 underline">INSCRIPTION :</h1>

                    <form action="ajout.php" method="post" class="flex flex-col text-center">
                        <label class="label font-bold"> Nom:</label>
                        <input type="text" name="nom" class="border-double border-4 border-light-blue-500 mx-5 md:mx-40 placeholder-blue-400 placeholder-opacity-50 px-4" placeholder="Nom (obligatoire)" value="<?php !empty($nom) ? print($nom) : print('') ?>" required />
                        <label class="label font-bold"> Prénom:</label>
                        <input type="text" name="prenom" class="border-double border-4 border-light-blue-500 mx-5 md:mx-40 placeholder-blue-400 placeholder-opacity-50 px-4" placeholder="Prenom (obligatoire)" value="<?php !empty($prenom) ? print($prenom) : print('') ?>" required />
                        <label class="label font-bold"> Mail:</label>
                        <input type="text" name="mail" class="border-double border-4 border-light-blue-500 mx-5 md:mx-40 placeholder-blue-400 placeholder-opacity-50 px-4" placeholder="Adresse Mail (obligatoire)" value="<?php !empty($mail) ? print($mail) : print('') ?>" required />
                        <label class="label font-bold"> Département:</label>
                        <input type="text" name="departement" class="border-double border-4 border-light-blue-500 mx-5 md:mx-40 placeholder-blue-400 placeholder-opacity-50 px-4" placeholder="Département (obligatoire)" value="<?php !empty($departement) ? print($departement) : print('') ?>" required />

                        <?php



                        // Affichage de la liste de sport disponible dans BDD
                        $data = findFullListSport();
                        ?>

                        <label class="label font-bold"> Sport:</label>
                        <select name="listsport" class="border-double border-4 border-light-blue-500 mx-20 md:mx-40">
                            <?php
                            for ($i = 0; $i < count($data); $i++) {
                            ?>
                                <option>
                                    <?php

                                    //Filtre de la récupération des données de la BDD
                                    print(filter_var($data[$i]["sport"], FILTER_SANITIZE_FULL_SPECIAL_CHARS));
                                    ?>
                                </option>
                            <?php
                            }
                            ?>
                        </select>
                        <label class="label font-bold">Niveau:</label>
                        <select name="listniveau" class="border-double border-4 border-light-blue-500 mx-20 md:mx-40">
                            <option class="" value="débutant" default>Débutant</option>
                            <option class="" value="confirmé">Confirmé</option>
                            <option class="" value="pro">Pro</option>
                            <option class="" value="supporter">Supporter</option>
                        </select>
                        <input type="submit" name="submit" class="rounded bg-pink-300 font-bold hover:bg-pink-400 transition delay-150 duration-300 ease-in-out transform hover:scale-110 h-10 w-20 my-10 self-center text-center">
                    </form>
                </div>


            </div>

            <?php

        else :

            // Validation des données du formulaire
            if (isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['mail']) && isset($_POST['departement']) && isset($_POST['listniveau']) && isset($_POST['listsport'])) {

                if (!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['mail']) && !empty($_POST['departement']) && !empty($_POST['listniveau']) && !empty($_POST['listsport']) && empty($_POST['autre'])) {

                    if (filter_var($_POST['nom'], FILTER_SANITIZE_FULL_SPECIAL_CHARS)) {
                        $_POST['nom'] = htmlspecialchars($_POST['nom'], ENT_NOQUOTES);

                        if (htmlspecialchars($_POST['prenom'], ENT_NOQUOTES)) {
                            $_POST['prenom'] = htmlspecialchars($_POST['prenom'], ENT_NOQUOTES);

                            if (filter_var($_POST["mail"], FILTER_VALIDATE_EMAIL)) {
                                $_POST["mail"] = filter_var($_POST["mail"], FILTER_SANITIZE_EMAIL);

                                if (filter_var($_POST['departement'], FILTER_SANITIZE_FULL_SPECIAL_CHARS)) {
                                    if($_POST['departement']>0 && $_POST['departement']<90){
                                    $_POST['departement'] = filter_var($_POST['departement'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

                                    if (filter_var($_POST['listsport'], FILTER_SANITIZE_FULL_SPECIAL_CHARS)) {
                                        $_POST['listsport'] = htmlspecialchars($_POST['listsport'], ENT_NOQUOTES);


                                        //Transaction ajout Personne
                                        if (!$bdd->inTransaction()) {
                                            try {
                                                $bdd->beginTransaction();
                                                //Verification si Id connu
                                                $mail = $_POST['mail'];
                                                $sport = $_POST["listsport"];
                                                $niveau = strtolower($_POST['listniveau']);


                                                //Vérification si id personne est connu
                                                $data = findPerson($mail);
                                                //Vérifier si l'id du sport est connu
                                                $DataSport = findIdSport($sport);


                                                if (!empty($data) && !empty($DataSport)) {
                                                    //L'email est déjà connu et le sport aussi
                                                    $idpersonne = $data['idpersonne'];
                                                    $idsport = $DataSport['idsport'];

                                                    $dataPrat = findIdSportIdPersonneInPratique($idpersonne, $idsport);
                                                    if (empty($dataPrat)) {

                                                        addAllIntoPratique($niveau, $idpersonne, $idsport);

                                                        $bdd->commit();
            ?>
                                                        <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                            <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-gray-200 rounded py-6">
                                                                <h2> Les informations suivantes ont été enregistrées:</h2>
                                                                <div><?php print($_POST['nom']) ?></div>
                                                                <div><?php print($_POST['prenom']) ?></div>
                                                                <div><?php print($_POST['listsport']) ?></div>
                                                                <div><?php print($_POST['listniveau']) ?></div>
                                                            </div>
                                                        </div>
                                                        <meta http-equiv='Refresh' content='3;URL=index.php'>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                            <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-gray-200 rounded py-6">
                                                                <h2> Vous êtes déjà inscrit à ce sport:</h2>
                                                                <div>Nom:<?php print($_POST['nom']) ?></div>
                                                                <div>Prénom:<?php print($_POST['prenom']) ?></div>
                                                                <div>Sport:<?php print($_POST['listsport']) ?></div>
                                                                <div>Niveau:<?php print($_POST['listniveau']) ?></div>
                                                            </div>
                                                        </div>
                                                        <meta http-equiv='Refresh' content='3;URL=ajout.php'>

                                                    <?php
                                                    }
                                                } else {
                                                    //Si email non connu rajout de la personne
                                                    $nom = $_POST['nom'];
                                                    $prenom = $_POST['prenom'];
                                                    $departement = $_POST['departement'];
                                                    $mail = strtolower($_POST['mail']);
                                                    $sport = $_POST['listsport'];
                                                    $niveau = strtolower($_POST["listniveau"]);

                                                    addPerson($nom, $prenom, $departement, $mail);

                                                    //On lance la fonction pour récupèrer l'id de la personne
                                                    $data = findPerson($mail);

                                                    $idpersonne = $data['idpersonne'];
                                                    //On lance la fonction pour récupèrer l'ID du sport sélectionné                                                    
                                                    $DataSport = findIdSport($sport);

                                                    //Ajout dans pratique IDsport et IDpersonne  récupérés                                   


                                                    $idsport = $DataSport["idsport"];


                                                    addAllIntoPratique($niveau, $idpersonne, $idsport);
                                                    $bdd->commit();

                                                    ?>
                                                    <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                        <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-gray-200 rounded py-6">
                                                            <h2> Les informations suivantes ont été enregistrées:</h2>
                                                            <div><?php print($_POST['nom']) ?></div>
                                                            <div><?php print($_POST['prenom']) ?></div>
                                                            <div><?php print($_POST['listsport']) ?></div>
                                                            <div><?php print($_POST['listniveau']) ?></div>
                                                        </div>
                                                    </div>
                                                    <meta http-equiv='Refresh' content='3;URL=index.php'>
                                            <?php
                                                }
                                            } catch (Exception $e) {
                                                $bdd->rollBack();
                                                print_r($bdd->errorInfo());
                                                die(var_dump("Error: " . $e->getMessage()));
                                            }
                                        } else {
                                            ?>
                                            <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                    <p>Oups, il y a déjà une transaction en cours...</p>
                                                    <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                    <p>Merci d'essayer plus tard</p>


                                                </div>
                                            </div>
                                            <meta http-equiv='Refresh' content='3;URL=ajout.php'>
                                        <?php
                                        }
                                    } else {
                                        ?>
                                        <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                            <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                <p>Oups, merci de sélectionner un sport existant dans la liste de sport...</p>
                                                <div><i class="fas fa-grin-beam-sweat"></i></div>

                                            </div>
                                        </div>
                                        <meta http-equiv='Refresh' content='3;URL=ajout.php'>
                                    <?php
                                    }
                                    }else{
                                                                            ?>
                                    <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                        <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                            <p>Oups, merci de saisir un numéro de département à deux chiffres en France métropolitaine...</p>
                                            <div><i class="fas fa-grin-beam-sweat"></i></div>
                                            <meta http-equiv='Refresh' content='3;URL=ajout.php'>
                                        <?php

                                    }
                                } else {
                                    ?>
                                    <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                        <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                            <p>Oups, merci de saisir un numéro de département à deux chiffres en France métropolitaine...</p>
                                            <div><i class="fas fa-grin-beam-sweat"></i></div>
                                            <meta http-equiv='Refresh' content='3;URL=ajout.php'>
                                        <?php

                                    }
                                } else {

                                        ?>
                                        <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                            <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                <p>Oups, merci de saisir une adresse mail valide</p>
                                                <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                <meta http-equiv='Refresh' content='3;URL=ajout.php'>
                                            <?php

                                        }
                                    } else {
                                            ?>
                                            <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bbg-red-500 rounded py-6">
                                                    <p>Oups, merci de saisir un prénom avec des caractères valides</p>
                                                    <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                    <meta http-equiv='Refresh' content='3;URL=ajout.php'>
                                                <?php
                                            }
                                        } else {
                                                ?>
                                                <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                    <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                        <p>Oups, merci de saisir un nom avec des caractères valides</p>
                                                        <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                        <meta http-equiv='Refresh' content='3;URL=ajout.php'>
                                                    <?php
                                                }
                                            } else {
                                                    ?>
                                                    <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                        <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                            <p>Oups, Merci de remplir tous les champs requis</p>
                                                            <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                            <meta http-equiv='Refresh' content='3;URL=ajout.php'>
                                                        <?php
                                                    }
                                                } else {

                                                    //SI $_POST["autre"] est vide on affiche le formulaire
                                                    if (empty($_POST["autre"])) {
                                                        ?>

                                                            <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                                <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                                    <p>Oups, Aucun nouveau sport n'a été rajouté</p>
                                                                    <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                                </div>
                                                            </div>
                                                            <meta http-equiv='Refresh' content='3;URL=ajout.php'>

                                                            <?php
                                                        } else {
                                                            if (isset($_POST["autre"])) {
                                                                if (htmlspecialchars($_POST["autre"], ENT_NOQUOTES)) {
                                                                    //on filtre quand même les données venant de la BDD
                                                                    $autre = htmlspecialchars(ucfirst($_POST["autre"]), ENT_NOQUOTES);

                                                                    // Préparation de la transaction s'il n'y en a pas en cours
                                                                    if (!$bdd->inTransaction()) {
                                                                        try {
                                                                            $bdd->beginTransaction();;
                                                                            $DataSport = findIdSport($autre);

                                                                            //si sport existant
                                                                            if (!empty($DataSport)) {
                                                            ?>
                                                                                <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                                                    <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                                                        <p>Oups, le sport saisi est déjà existant</p>
                                                                                        <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                                                    </div>
                                                                                </div>
                                                                                <meta http-equiv='Refresh' content='3;URL=ajout.php'>

                                                                            <?php

                                                                                //si pas de sport existant
                                                                            } else {

                                                                                addSport($autre);
                                                                                $bdd->commit();
                                                                            ?>
                                                                                <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                                                    <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-gray-200 rounded py-6">
                                                                                        <h2>Le sport <?php print($autre) ?> a été ajouté à la liste, vous pouvez vous inscrire maintenant.</h2>
                                                                                    </div>
                                                                                </div>
                                                                                <meta http-equiv='Refresh' content='3;URL=index.php'>

                                                                        <?php
                                                                            }
                                                                        } catch (Exception $e) {
                                                                            $bdd->rollBack();
                                                                            print_r($bdd->errorInfo());
                                                                            die(printf("Error: " . $e->getMessage()));
                                                                        }
                                                                    } else {
                                                                        ?>
                                                                        <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                                            <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                                                <p>Oups, un problème est survenu merci d'essayer plus tard</p>
                                                                                <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                                            </div>
                                                                        </div>
                                                                        <meta http-equiv='Refresh' content='3;URL=ajout.php'>

                                                                    <?php
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                                        <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">>
                                                                            <p>Oups, merci de saisi du nouveau sport</p>
                                                                            <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                                        </div>
                                                                    </div>
                                                                    <meta http-equiv='Refresh' content='3;URL=ajout.php'>
                                                                <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                                    <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                                        <p class="text-center">Oups, merci de saisi du nouveau sport</p>
                                                                        <div class="text-center"><i class="fas fa-grin-beam-sweat"></i></div>
                                                                    </div>
                                                                </div>
                                                                <meta http-equiv='Refresh' content='3;URL=ajout.php'>
                                                <?php
                                                            }
                                                        }
                                                    }


                                                endif;

                                                ?>

        </section>
</body>

</html>