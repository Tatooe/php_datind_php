<?php
//Centralisation des requête SQL


/**
 * Retourne la conection à la BDD
 */

function getPDO()
{
    try {
        $bdd = new PDO(DB_DRIVER . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DEFAULT_DB_LOGIN, DEFAULT_DB_PWD, array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        ));
    } catch (Exception $e) {
        die('Erreur:' . $e->getMessage());
    }

    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $bdd;
}

/**
 * Retourne la liste des sports
 * 
 * @return array
 */
function findFullListSport(): array
{
    $bdd = getPDO();
    $MyReqFullListSport = $bdd->prepare(REQ_FULL_LIST_SPORT);
    $MyReqFullListSport->execute();
    $data = $MyReqFullListSport->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}

/**
 * Retourne les informations de la personne par $mail
 * 
 * @return array 
 * @return null
 */

function findAllPerMail(string $mail)
{
    $bdd = getPDO();
    //préparation de la requête 
    $MyReqAllPerMail = $bdd->prepare(REQ_ALL_PAR_MAIL);
    $MyReqAllPerMail->execute([":mail" => $mail]);
    $data = $MyReqAllPerMail->fetch();
    return $data;
}

/**
 * Retourne tout les informations des tables personne, pratiuqe, sport en fonction de l'email $mail
 * 
 * @return array
 */
function findAllTablesPerMail(string $mail): array
{

    $bdd = getPDO();
    // Préparation requête pour afficher les sports correspondant à la personne 
    $MyReqFindAllPerPersonne = $bdd->prepare(REQ_FIND_ALL_PER_PERS);

    $MyReqParams = [
        ":mail" => $mail,
    ];
    $MyReqFindAllPerPersonne->execute($MyReqParams);
    $data = $MyReqFindAllPerPersonne->fetchAll(PDO::FETCH_ASSOC);

    return $data;
}

/**
 * function permettant de trouver tous les départements disponible dans la BDD
 * 
 * @return array
 */
function findListDpt(): array
{
    $bdd = getPDO();
    $MyReqFindAllDpt = $bdd->prepare(REQ_FIND_ALL_DPT);
    $MyReqFindAllDpt->execute();
    $data = $MyReqFindAllDpt->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}

/**
 * function permettant de trouver toutes les personnes en filtrant par niveau, sport et département
 * 
 * @return array
 */
function findAllPerSptNivDpt(string $niveau, string $sport, string $departement): array
{
    $bdd = getPDO();

    $MyReqFindAllPerSptNivDpt = $bdd->prepare(REQ_FIND_ALL_PER_SPT_NIV_DPT);
    $MyReqParams = [
        ":niveau" => $niveau,
        ":sport" => $sport,
        ":departement" => $departement
    ];
    $MyReqFindAllPerSptNivDpt->execute($MyReqParams);
    $data = $MyReqFindAllPerSptNivDpt->fetchAll(PDO::FETCH_ASSOC);

    return $data;
}
/**
 * function permettant de trouver une personne par son mail
 * 
 *  @return array 
 * @return null
 */
function findPerson(string $mail)
{

    $bdd = getPDO();
    //Requête de recherche de ID de personne pour ajouter dans niveau
    $MyReqFindIdPerson = $bdd->prepare(REQ_FIND_ID_PERSON);

    $MyReqParams = [
        ":mail" => $mail
    ];

    $MyReqFindIdPerson->execute($MyReqParams);
    $data = $MyReqFindIdPerson->fetch();

    return $data;
}

/**
 * function permettant d'effectuer une requête pour trouver le sport en fonction du nom du sport
 * 
 * @return array 
 * @return null
 */

function findIdSport(string $sport)
{
    $bdd = getPDO();

    $MyReqFindIdSport = $bdd->prepare(REQ_FIND_ID_SPORT);
    $MyReqParams = [
        ":sport" => $sport
    ];
    $MyReqFindIdSport->execute($MyReqParams);
    $DataSport = $MyReqFindIdSport->fetch();

    return $DataSport;
}

/**
 * function qui permet de vérifier dans la table pratique si les lignes existes déjà (hormis l'id pratique) pour éviter les doublons
 * 
 * * @return array 
 * @return null
 */

function findIdSportIdPersonneInPratique(string $idpersonne, string $idsport)
{
    $bdd = getPDO();
    $MyReqFindIdpersIdsportInPrat = $bdd->prepare(REQ_FIND_ID_PERS_ID_SPRT_IN_PRATIQ);
    $MyReqParams = [
        ":idsport" => $idsport,
        ":idpersonne" => $idpersonne
    ];
    $MyReqFindIdpersIdsportInPrat->execute($MyReqParams);
    $DataPrat = $MyReqFindIdpersIdsportInPrat->fetchAll(PDO::FETCH_ASSOC);

    return $DataPrat;
}

/**
 * function permattant de rajouter un id sport connu et un id personne connu dans le tableau associatif pratique
 * 
 * 
 */
function addAllIntoPratique(string $niveau, string $idpersonne, string $idsport): void
{
    $bdd = getPDO();
    $MyReqAddAllPratique = $bdd->prepare(REQ_ADD_ALL_PRATIQUE);
    $MyReqParams = [
        ":niveau" => $niveau,
        ":idpersonne" => $idpersonne,
        ":idsport" => $idsport
    ];
    //On rajoute  l'idpersonne et idsport dans le tableau pratique qui fait le lien
    $MyReqAddAllPratique->execute($MyReqParams);
}

/**
 * function rajout d'une personne
 * 
 */
function addPerson(string $nom, string $prenom, string $departement, string $mail): void
{
    $bdd = getPDO();
    $MyReqAddPerson = $bdd->prepare(REQ_ADD_PERSON);
    $MyReqParams = [
        ":nom" => $nom,
        ":prenom" => $prenom,
        ":departement" => $departement,
        ":mail" => $mail
    ];
    $MyReqAddPerson->execute($MyReqParams);
}

/**
 * Function ajout sport
 */

function addSport(string $autre): void
{
    $bdd = getPDO();
    $MyReqAddSport = $bdd->prepare(REQ_ADD_SPORT);
    $MyReqParams = [
        ":sport" => ucfirst($autre)
    ];

    $MyReqAddSport->execute($MyReqParams);
}

/**
 * Function pour mettre � jour les donn�es de l'utilisateur
 */



/**
 * Function pour supprimer un utilisateur par rapport � son mail
 */
function deletePers(string $mail): void
{
    $bdd = getPDO();
    $MyReqDeletePers = $bdd->prepare(REQ_DELETE_ALL_BY_PERSONNE);
    $MyReqParams = [
        ":mail" => $mail,
    ];
            
    $MyReqDeletePers->execute($MyReqParams);
}