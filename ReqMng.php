<?php

/**
 * Requête pour avoir la liste des sports existant dans la base
 */

 const REQ_FULL_LIST_SPORT = "SELECT sport FROM sport ORDER BY sport";

 /**
  * Requête pour récupérer le prénom par rapport à l'email
  */
  const REQ_ALL_PAR_MAIL = "SELECT * FROM personne WHERE mail = :mail";

  /**
   * Requête pour récupérer tous les départements existants dans la BDD
   */

   const REQ_FIND_ALL_DPT = "SELECT DISTINCT departement FROM personne ORDER BY departement";

/**
 * Requête pour enregistrer le nom, prenom, mail, departement dans personne
 */

 const REQ_ADD_PERSON ="INSERT INTO personne(nom, prenom, departement, mail) VALUES (:nom, :prenom, :departement, :mail)";
/**
 * Requête pour vérifier si le sport est présent
 */
 const REQ_FIND_SPORT ="SELECT sport FROM sport WHERE sport = :autre ";
 /** 
  * Requête pour trouver les sports conrespond à une personne en particulier
 */
const REQ_FIND_ALL_PER_PERS ="SELECT * FROM personne,pratique,sport WHERE personne.idpersonne = pratique.idpersonne AND pratique.idsport = sport.idsport AND personne.mail = :mail";
 /**
  * Requête pour ajouter un nouveau sport dans sport
  */
  const REQ_ADD_SPORT ="INSERT INTO sport(sport) VALUES(:sport)";
    /**
   * Recherche idpersonne dans table personne
   */
  const REQ_FIND_ID_PERSON ="SELECT idpersonne FROM personne WHERE mail = :mail";

  /**
   * Recherche idsport dans table sport
   */
  const REQ_FIND_ID_SPORT ="SELECT idsport FROM sport WHERE sport = :sport";
  /**
   * Recherche niveau pratique suivant idsport et idpersonne dans pratique
   */
  const REQ_FIND_NIVEAU = "SELECT niveau FROM pratique WHERE idsport= :idsport AND idpersonne= :idpersone";
  /**
   * Ajout relation entre idsport/idpersonne et niveau dans table pratique
   */
  const REQ_ADD_ALL_PRATIQUE ="INSERT INTO pratique(niveau, idpersonne, idsport) VALUES (:niveau, :idpersonne, :idsport)";

  /**
   * Requête pour avoir la liste complête de la BDD avec comme critères le sport, le niveau et le departement
   */

   const REQ_FIND_ALL_PER_SPT_NIV_DPT = "SELECT * FROM  personne,pratique,sport WHERE personne.idpersonne = pratique.idpersonne AND pratique.idsport = sport.idsport AND sport.sport=:sport AND personne.departement= :departement AND pratique.niveau = :niveau";

/**
 * requête qui permet de vérifier dans la table pratique si les lignes existes déjà (hormis l'id pratique) pour éviter les doublons
 * 
 */   
   const REQ_FIND_ID_PERS_ID_SPRT_IN_PRATIQ = "SELECT idpersonne, idsport FROM pratique WHERE idpersonne = :idpersonne AND idsport = :idsport";
   
   /**
    * Requ�te qui permet d'updater les informations de l'utilisateur(nom, prenom, mail, departement de la table personne et Niveau de la table pratique) suivant l'idpersonne et l'idsport
    * 
    */
   const REQ_UPDATE_PERSON_NIVEAU = "UPDATE personne, pratique SET nom = :newNom, prenom= :newPrenom, departement= :newDepartement, mail = :newMail,  niveau= :newNiveau WHERE personne.idpersonne = :idpersonne and pratique.idpratique= :idpratique and pratique.idsport = :idsport";
   
   /**
    * Requ�te pour supprimer une personne (cascade dans la BDD)
    */
   
   const REQ_DELETE_ALL_BY_PERSONNE = "DELETE personne, pratique FROM personne INNER JOIN pratique ON personne.idpersonne = pratique.idpersonne where personne.mail = :mail";