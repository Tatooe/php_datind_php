<?php
require_once "ServerParam.php";
require_once "ReqMng.php";
require_once "req.php";
require_once "navbar.php";

//initialisation des cookies à vide lors de la connexion à la page d'accueil
setcookie("mail", '', time() + 60 * 60 * 24 * 30);
setcookie("nom", '', time() + 60 * 60 * 24 * 30);
setcookie("prenom", '', time() + 60 * 60 * 24 * 30);
setcookie("departement", '', time() + 60 * 60 * 24 * 30);

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.0.2/tailwind.min.css" integrity="sha512-+WF6UMXHki/uCy0vATJzyA9EmAcohIQuwpNz0qEO+5UeE5ibPejMRdFuARSrl1trs3skqie0rY/gNiolfaef5w==" crossorigin="anonymous" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./style/style.css" />
    <title>PHP DATING</title>
</head>

<body class="flex flex-col content-center text-center ">



    <?php
    navbar();

    if (empty($_POST)) :

    ?>
        <section>
            <div class="grid grid-cols-8 gap-4 mt-20 md:mt-10">
                <div class="col-start-2 col-span-6 my-4 md:col-start-3 md:col-span-4 bg-yellow-100 rounded md:my-10 pb-5 md:pb-30 shadow-md">
                    <h2 class="mt-4 font-bold">Identifiez vous</h2>
                    <form action="index.php" method="post" class="mt-1 md:mb-10">
                        <input type="text" name="mail" class="border rounded border-blue-300 focus:outline-none focus:ring-2 focus:ring-blue-600 shadow-md placeholder-blue-400 placeholder-opacity-50 ml-2 px-4" placeholder="Votre mail..." />
                        <input type="submit" name="submit" class="rounded bg-blue-300 hover:bg-blue-400 px-2 m-2 md:w-20 h-7" />
                    </form>
                </div>
            </div>

            <a href="./ajout.php"><button class="rounded bg-pink-300 font-bold hover:bg-pink-400 transition delay-150 duration-300 ease-in-out transform hover:scale-110 w-auto h-10 px-4 my-10 md:my-5 ">INSCRIPTION</button></a>
            <?php


            //Requête pour la liste complète des sports
            $data = findFullListSport();

            ?>
            <div class="text-center md:grid md:grid-rows-5 md:grid-cols-4 hidden">
                <?php
                // Affichage de la liste de sport disponible dans BDD
                for ($i = 0; $i < count($data); $i++) {
                ?>
                    <div class="box-content h-20 w-auto bg-gray-200 rounded transform scale-110 -rotate-6 text-center shadow-md mx-10 mt-10 transition duration-500 ease-in-out hover:bg-yellow-600 transform hover:-translate-y-1 hover:scale-110 hover:rotate-3">
                        <div class="my-5 font-bold sport">

                            <?php
                            //Filtre de la récupération des données de la BDD

                            print(filter_var($data[$i]["sport"], FILTER_SANITIZE_FULL_SPECIAL_CHARS));
                            ?>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>

        <?php



    else :
        ?>
            <?php
            if (isset($_POST["mail"])) {
                if (!empty($_POST["mail"])) {
                    if (filter_var($_POST["mail"], FILTER_VALIDATE_EMAIL)) {
                        $_POST["mail"] = filter_var($_POST["mail"], FILTER_SANITIZE_EMAIL);
                        //initialisation des variables
                        $mail = $_POST['mail'];
                        //requête pour avoir la liste des sports par mail
                        $data = findAllPerMail($mail);

                        if (!empty($data)) {
            ?>
                            <section class="h-100">
                                <div class="grid grid-cols-8 gap-4 mb-100">
                                    <div class="col-start-2 col-span-6 md:col-start-3 md:col-span-4 bg-gray-200 rounded mt-10 pb-30 shadow-md">
                                        <h2 class="font-bold my-5 ">
                                            <?php
                                            print("Bonjour " . strtoupper(filter_var($data["prenom"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)));
                                            //initialisation des variables
                                            $nom = $data["nom"];
                                            $prenom = $data["prenom"];
                                            $departement = $data["departement"];
                                            $departement = str_pad($departement, 2, "0", STR_PAD_LEFT);
                                            //enregistrement du mail dans un cookie
                                            setcookie("mail", (string) $mail, time() + 60 * 60 * 24 * 30);
                                            setcookie("nom", $nom, time() + 60 * 60 * 24 * 30);
                                            setcookie("prenom", $prenom, time() + 60 * 60 * 24 * 30);
                                            setcookie("departement", $departement, time() + 60 * 60 * 24 * 30);

                                            ?>
                                        </h2>
                                        <p>Content de te revoir !</p>
                                        <div><i class="fas fa-laugh-beam hover:text-yellow-400"></i></div>
                                        <p>Que veux-veux tu faire aujourd'hui?</p>

                                        <div><a href='./ajout.php'><button class="rounded bg-pink-300 font-bold hover:bg-pink-400 transition delay-150 duration-300 ease-in-out transform hover:scale-110 m-2 p-2 w-auto md:h-10 md:px-4 md:my-4">INSCRIPTION POUR UN AUTRE SPORT</button></a></div>
                                        <div><a href='./recherche.php'><button class="rounded bg-blue-300 font-bold hover:bg-blue-auto0 transition delay-150 duration-300 ease-in-out transform hover:scale-110 m-2 p-2 w-auto md:h-10 md:px-4 md:my-4">RECHERCHER UN PARTENAIRE</button></a></div>
                                    </div>
                                </div>
                            </section>
                        <?php
                        } else {
                            //Les cookies sont à vide si mail non connu
                            setcookie("mail", (string) '', time() + 60 * 60 * 24 * 30);
                            setcookie("nom", '', time() + 60 * 60 * 24 * 30);
                            setcookie("prenom", '', time() + 60 * 60 * 24 * 30);
                            setcookie("departement", '', time() + 60 * 60 * 24 * 30);
                            header('Location: ./ajout.php');
                            exit();
                        }
                    } else {
                        ?>
                        <div class="grid grid-cols-8 gap-4 mb-100">
                            <div class="col-start-3 col-span-4 bg-red-500 rounded mt-10 pb-30 shadow-md">
                                <p>Oups, merci de saisir une adresse mail valide</p>
                                <div><i class="fas fa-grin-beam-sweat"></i></div>
                            </div>
                        </div>
                        <meta http-equiv='Refresh' content='3;URL=index.php'>
                    <?php
                    }
                } else {
                    ?>
                    <div class="grid grid-cols-8 gap-4 mb-100">
                        <div class="col-start-3 col-span-4 bg-red-500 rounded mt-10 pb-30 shadow-md">
                            <p>Oups, merci de renseigner une adresse mail</p>
                            <div><i class="fas fa-grin-beam-sweat"></i></div>
                        </div>
                    </div>
                    <meta http-equiv='Refresh' content='3;URL=index.php'>
                <?php
                }
            } else {
                ?>
                <div class="grid grid-cols-8 gap-4 mb-100">
                    <div class="col-start-3 col-span-4 bg-red-500 rounded mt-10 pb-30 shadow-md">
                        <p>Oups, merci de saisir les données demandées</p>
                        <div><i class="fas fa-grin-beam-sweat"></i></div>
                    </div>
                </div>
                <meta http-equiv='Refresh' content='3;URL=index.php'>
            <?php
            }
            ?>


        <?php

    endif;
        ?>
        </section>
</body>

</html>