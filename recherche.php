<?php
require_once "ServerParam.php";
require_once "ReqMng.php";
require_once "req.php";
require_once "navbar.php";

if (!empty($_COOKIE['mail']) && !empty($_COOKIE['nom']) && !empty($_COOKIE['prenom']) && !empty($_COOKIE['departement'])) {
    $nom = $_COOKIE['nom'];
    $prenom = $_COOKIE['prenom'];
    $mail = $_COOKIE['mail'];
    $departement = $_COOKIE['departement'];
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.0.2/tailwind.min.css" integrity="sha512-+WF6UMXHki/uCy0vATJzyA9EmAcohIQuwpNz0qEO+5UeE5ibPejMRdFuARSrl1trs3skqie0rY/gNiolfaef5w==" crossorigin="anonymous" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./style/style.css" />
    <title>PHP DATING</title>
</head>


<body class="flex flex-col content-center text-center ">


    <?php
    if(empty($_POST['delete'])):
    
    navbar();

    //Retourne tout les informations des tables personne, pratiuqe, sport en fonction de l'email $mail
    $data = findAllTablesPerMail($mail);
    ?>
    <section>
        <aside>
            <?php
            count($data) > 1 ? print("<h2 class='text-white font-bold my-4'>Tes sports: </h2>") : print("<h2 class='text-white font-bold '>Ton sport: </h2>")
            ?>
            <div class="flex flex-wrap justify-center">
                <?php
                for ($i = 0; $i < count($data); $i++) {
                ?>
                    <div class="box-content h-20 w-22 bg-gray-200 rounded transform scale-110 -rotate-6 text-center shadow-md px-2 mx-2 my-1 md:mx-10 md:mt-10 transition duration-500 ease-in-out hover:bg-yellow-600 transform hover:-translate-y-1 hover:scale-110 hover:rotate-3">
                        <div class="my-5 font-bold sport">
                            <div>
                                <?php
                                print($data[$i]["sport"]);
                                ?>
                            </div>
                            <div>
                                <?php
                                print($data[$i]["niveau"]);
                                ?>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </aside>

        <?php

        // Affichage de la liste de sport disponible dans BDD
        $data = findFullListSport();
        ?>
        <aside class="mt-5">
            <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mb-100">
                <div class="col-start-2 col-span-10 md:col-start-2 md:col-span-6 bg-gray-800 rounded shadow-md mt-10 py-10">
                    <h2 class="text-white font-bold ">Trouver le partenaire qui vous correspond : </h2>
                    <form action="recherche.php" method="post" class="my-5 rounded flex flex-col text-left p-4 md:flex-row md:justify-center">

                        <label class="font-bold text-gray-100 mr-4">Sport existant:</label>
                        <select name="listsport" class="rounded border border-4 border-light-blue-500 m-2 md:mr-6">
                            <?php
                            for ($i = 0; $i < count($data); $i++) {
                            ?>
                                <option>
                                    <?php

                                    //Filtre de la récupération des données de la BDD
                                    print(htmlspecialchars($data[$i]["sport"], ENT_NOQUOTES));
                                    ?>
                                </option>
                            <?php
                            }
                            ?>
                        </select>

                        <label class="font-bold text-gray-100 mr-4">Le niveau souhaité:</label>
                        <select name="listniveau" class="rounded border border-4 border-light-blue-500 m-2 md:mr-6">
                            <option value="débutant" default>débutant</option>
                            <option value="confirmé">confirmé</option>
                            <option value="pro">pro</option>
                            <option value="supporter">supporter</option>
                        </select>


                        <?php
                        $data = findListDpt();
                        ?>

                        <label class="font-bold text-gray-100 mr-4">Dans quel département ? </label>
                        <select name="listdpt" class="rounded border border-4 border-light-blue-500 m-2 md:mr-6">
                            <?php
                            for ($i = 0; $i < count($data); $i++) {
                            ?>
                                <option class="text-black">
                                    <?php

                                    //Filtre de la récupération des données de la BDD
                                    print(htmlspecialchars(str_pad($data[$i]["departement"], 2, "0", STR_PAD_LEFT), ENT_NOQUOTES));
                                    ?>
                                </option>
                            <?php
                            }

                            ?>
                        </select>
                        <button type="submit" name="validate" class="rounded bg-blue-300 hover:bg-blue-400 mt-5 md:ml-4 w-20 h-7 self-center">Rechercher</button>

                    </form>
                </div>
            </div>
        </aside>


        <?php





        if (!empty($_POST['listniveau'])&& !empty($_POST['listsport'])&& !empty($_POST['listdpt'])) {

            $niveau = htmlspecialchars($_POST["listniveau"], ENT_NOQUOTES);
            $sport = htmlspecialchars($_POST["listsport"], ENT_NOQUOTES);
            $departement = htmlspecialchars($_POST["listdpt"], ENT_NOQUOTES);
            
            //demarrage de la requête trouver toutes les personnes en filtrant par niveau, sport et département
            $data = findAllPerSptNivDpt($niveau, $sport, $departement);

            if (!empty($data)) {
        ?>
                <div class="grid grid-cols-12 md:grid-cols-8 md:gap-4 mt-5">
                    <div class="col-start-1 col-span-12 mx-2 md:col-start-3 md:col-span-4 shadow-md bg-gray-200 rounded py-6">
                        <h2 class="font-bold ">Les personnes vous correspondants:</h2>
                        <table class="m-1 md:m-5 table-fixed border-collapse">
                            <thead class="bg-purple-300">
                                <tr class="">
                                    <th class="w-1/5 border border-purple-600">Prénom</th>
                                    <th class="w-1/5 border border-purple-600">Nom</th>
                                    <th class="w-2/5 border border-purple-600">Mail</th>
                                    <th class="w-1/5 border border-purple-600">Sport</th>
                                    <th class="w-2/5 border border-purple-600">Niveau</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($data); $i++) {
                                ?>
                                    <tr>
                                        <td class="border border-purple-600 text-xs md:text-sm">
                                            <?php print(htmlspecialchars(($data[$i]["prenom"]), ENT_NOQUOTES)) ?>
                                        </td>
                                        <td class="border border-purple-600 text-xs md:text-sm">
                                            <?php print(htmlspecialchars($data[$i]["nom"], ENT_NOQUOTES)) ?>
                                        </td>
                                        <td class="border border-purple-600 text-xs md:text-sm">
                                            <?php print(htmlspecialchars($data[$i]["mail"], ENT_NOQUOTES)) ?>
                                        </td>
                                        <td class="border border-purple-600 text-xs md:text-sm">
                                            <?php print(htmlspecialchars($data[$i]["sport"], ENT_NOQUOTES)) ?>
                                        </td>
                                        <td class="border border-purple-600 text-xs md:text-sm">
                                            <?php print(htmlspecialchars($data[$i]["niveau"], ENT_NOQUOTES)) ?>
                                        </td>
                                    </tr>
                                <?php


                                }
                            } else {
                                ?>
                                <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                    <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-gray-200 rounded py-6">
                                        <h2 class="font-bold ">Désolé, personne ne correspondant à votre recherche.</h2>
                                        <div><i class="fas fa-grin-beam-sweat"></i></div>
                                        <ul>
                                            <li><strong>Sport: &nbsp </strong> <?php print($_POST["listsport"]) ?></li>
                                            <li><strong>Niveau: &nbsp</strong> <?php print(ucfirst($_POST["listniveau"])) ?></li>
                                            <li><strong>Département: &nbsp</strong> <?php print($_POST["listdpt"]) ?></li>
                                            <ul>
                                        <?php

                                    }
                                }
                                        ?>
                            </tbody>
                        </table>
                    </div>
                </div>



                <div>
                    <div><button class="rounded bg-pink-300 font-bold hover:bg-pink-400 transition delay-150 duration-300 ease-in-out transform hover:scale-110 w-auto h-10 px-4 mt-10" name="incript"><a href='./ajout.php'>INSCRIPTION POUR UN AUTRE SPORT </a></button></div>
                    
                    <div><button class="rounded bg-blue-300 font-bold hover:bg-blue-400 transition delay-150 duration-300 ease-in-out transform hover:scale-110 w-auto h-10 px-4 my-4" name="accueil"><a href='./index.php'>DECONNEXION</a></button></div>
                
                    <form action="recherche.php" method="post" name="delete">
                        <input type="hidden" name="delete" value="<?php print($mail) ?>">
                        <input type="submit" name="delete_Pers" class="rounded bg-red-700 font-bold hover:bg-red-900 transition delay-150 duration-300 ease-in-out transform hover:scale-110 w-auto h-10 px-4 my-4" value="DESINSCRIPTION"> </input>
                    </form>
                <?php
                             
                
             else:                 
                if(!empty($_POST["delete"])){
                    if(isset($_POST["delete"])){
                        
                        $bdd = getPDO();
                                          
                        
                if (!$bdd->inTransaction()) {
                    try {
                        $bdd->beginTransaction();
                        deletePers($mail);
                        $bdd->commit();
                        ?>
                                            <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                    <p>Vous allez nous manquer <?php print($prenom) ?></p>
                                                    <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                    <p>Merci d'essayer plus tard</p>
                                                    <meta http-equiv='Refresh' content='3;URL=index.php'>
                                                </div>
                                            </div>
                        <?php
                        print('<h1 class="text-white font-bold">Vous avez bien été désinscrit ,</br> En espérant vous revoir... '.$prenom.'</h1>');
                        ?><meta http-equiv='Refresh' content='3;URL=index.php'>
                        <?php
                }catch(Exception $e){
                    $bdd->rollBack();
                    print_r($bdd->errorInfo());
                    die(printf("Error: " . $e->getMessage()));
                }
                }
                }else{
                    ?>
                        <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                    <p>Oups, Probléme de nom de l'input DELETE</p>
                                                    <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                    <p>Merci d'essayer plus tard</p>
                                                    <meta http-equiv='Refresh' content='3;URL=index.php'>
                                                </div>
                                            </div>
                        <?php
                    
                }
                }else{
                                        ?>
                        <div class="grid grid-cols-12 md:grid-cols-8 gap-4 mt-5">
                                                <div class="col-start-2 col-span-10 md:col-start-3 md:col-span-4 shadow-md bg-red-500 rounded py-6">
                                                    <p>Oups, Probléme de champ vide DELETE</p>
                                                    <div><i class="fas fa-grin-beam-sweat"></i></div>
                                                    <p>Merci d'essayer plus tard</p>
                                                    <meta http-equiv='Refresh' content='3;URL=index.php'>
                                                </div>
                                            </div>
                        <?php
                   
                }
                endif;
                ?>
                </div>
                </div>
                </div>
    </section>
</body>

</html>