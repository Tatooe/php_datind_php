<?php

function navbar()
{

?>
    <header>
        <div class="box-content h-20 w-100 pt-4 bg-opacity-75 bg-indigo-100 flex flex-nowrap justify-around overflow-hidden">

            <div><i class="fas fa-running icon animate-pulse text-sm md:text-2xl "></i></div>
            <div><i class="fas fa-skiing icon animate-pulse text-sm md:text-2xl"></i></div>
            <div><i class="fas fa-biking icon animate-pulse text-sm md:text-2xl"></i></div>
            <div><i class="fas fa-quidditch icon animate-pulse text-sm md:text-2xl"></i></div>
            <div>
                <h1 class="text-sm md:text-2xl">THE site de rencontre Sportive!</h1>
            </div>
            <div><i class="fas fa-volleyball-ball icon animate-pulse text-sm md:text-2xl"></i></div>
            <div><i class="fas fa-skating icon animate-pulse text-sm md:text-2xl"></i></div>
            <div><i class="fas fa-football-ball icon animate-pulse text-sm md:text-2xl"></i></div>
            <div><i class="fas fa-swimmer icon animate-pulse text-sm md:text-2xl "></i></div>


        </div>
    </header>
<?php



}
